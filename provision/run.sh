#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

function distro_id () {
    lsb_release -is 2> /dev/null | awk '{print tolower($0)}'
}

function ubuntu_codename() {
    if is_debian; then
        DEBIAN_RELEASE=$(lsb_release -rs 2> /dev/null)

        case $DEBIAN_RELEASE in
            10) echo "bionic" ;;
            11) echo "focal" ;;
            12 | n/a) echo "jammy" ;;
        esac
        return
    fi

    lsb_release -cs 2> /dev/null | awk '{print tolower($0)}'
}

function distro_release () {
    lsb_release -rs 2> /dev/null
}

function is_ubuntu () {
    [ $(distro_id) == "ubuntu" ] || [ $(distro_id) == "pop" ]
}

function is_debian () {
    [ $(distro_id) == "debian" ] || [ $(distro_id) == "raspbian" ]
}

function add_src_deb822 () {
    if [ -f ${1} ]; then
        echo "Adding deb-src sources to ${1}"
        ${SUDO} sed -i '/^Types: deb$/ s/$/ deb-src/g' ${1}
    fi
}

SUDO=""
SUDO_NO_ENV=""

if [ "$EUID" -eq 0 ]; then
    echo "Ran as root"
else
    echo 'Ran as non-root user'
    SUDO="sudo -E -u root --"
    SUDO_NO_ENV="sudo -u root --"
fi

apt_dir=/etc/apt
sources_dir=${apt_dir}/sources.list.d

echo -e "\nConfiguring apt sources"

if [ -f ${sources_dir}/debian.sources ]; then
    echo "> Found 'debian.sources' file"

    add_src_deb822 ${sources_dir}/debian.sources

elif [ -f ${sources_dir}/ubuntu.sources ]; then
    echo "> Found 'ubuntu.sources' file"

    add_src_deb822 ${sources_dir}/ubuntu.sources
else
    echo "> Falling back to using 'sources.list' file"

    echo "Ensuring deb-src lines are uncommented"
    ${SUDO} sed -i '/deb-src/s/^# //g' ${apt_dir}/sources.list
fi


echo -e "\nEnsuring ansible is installed"

if is_ubuntu || is_debian; then
    echo -e "\n Detected Debian-like system"

    if ! command -v pipx; then
        echo "Installing pipx"
        ${SUDO} apt-get update
        ${SUDO} apt-get install -y software-properties-common pipx
    fi

    if ${SUDO} test ! -f /root/.local/bin/ansible; then
        echo "Installing ansible with pipx"
        ${SUDO_NO_ENV} pipx install --include-deps ansible
        ${SUDO_NO_ENV} pipx inject ansible python-debian requests
    fi

    ${SUDO_NO_ENV} pipx upgrade ansible
else
    echo "Unsuported distro" $(distro_id)
    exit 1
fi

cd $DIR


${SUDO} /root/.local/bin/ansible-galaxy collection install -r requirements.yml
echo -e "\nRunning playbook"
${SUDO} /root/.local/bin/ansible-playbook setup.yml \
        --connection=local \
        --inventory=host-features.yml "$@"
